package com.example.demo.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EntryController {
    @GetMapping("/hello")
    public String entryPoint() {
        return "Hello Entry Point! This is from the dev branch";
    }
}
